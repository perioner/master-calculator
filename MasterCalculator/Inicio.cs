﻿using System;

namespace MasterCalculator
{
    class Inicio
    {
        static void Main(string[] args)
        {
            MasterCalculator.Inicio menu = new MasterCalculator.Inicio();
            menu.Menu();
        }

        public void Menu()
        {
            bool menu = false;

            do
            {
                Console.WriteLine("Welcome to Master Calculator");
                Console.WriteLine("In this simple Console program, you can compete your mind against the machine's");
                Console.WriteLine("1) Practice (4 tests | 10 hits in each | addition and subtraction)");
                Console.WriteLine("2) Medium (Not Work in this version)");
                Console.WriteLine("3) Hight (Not Work in this version)");
                Console.WriteLine("4) Hardcore (Not Work in this version)");
                Console.WriteLine("5) Exit");

                Console.Write("Choose an option from 1 to 5: "); int opcion = int.Parse(Console.ReadLine());

                switch (opcion)
                {
                    case 1:
                        MasterCalculator.EasyLevel lvl = new MasterCalculator.EasyLevel();
                        Console.WriteLine("Standards of practice");
                        Console.WriteLine("1) There are 4 tests of 10 questions each.");
                        Console.WriteLine("2) Every fault is a mistake.");
                        Console.WriteLine("3) Upon reaching the 10 correct questions, an average grade will be displayed");
                        Console.WriteLine("4) Share your note on our website MasterCalculator.wow-cms.com");
                        lvl.iniciarLevelEasy(1);
                        break;
                    case 2:

                        break;
                    case 3:

                        break;
                    case 4:

                        break;
                    case 5:

                        break;
                }

            } while (menu == false);
        }
    }
}
