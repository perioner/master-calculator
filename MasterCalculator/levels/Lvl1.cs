﻿using System;

namespace MasterCalculator
{
    public class Practice
    {
        /*
         * Private Variables int
         * using for Calculator and Wave control
        */
        private static int total, levelis = 10, resultado, Aciertos = 0, user, Level = 1;
        
        /*
        * Private Variables double
        * Using for Score Puntuation
        */
        private static double score = 0, scorear = 1, scorefail = 0.5;
        
        public void InitPractice()
        {
            bool levelcomplete = false;

            do
            {
                Random r = new Random();

                int sum1 = r.Next(10, 101);
                int sum2 = r.Next(10, 101);
                int opcion = r.Next(1, 3);


                switch (opcion)
                {
                    case 1:
                        resultado = sum1 + sum2;
                        Console.Write("How much is " + sum1 + " + " + sum2 + " ? ");
                        user = int.Parse(Console.ReadLine());
                        break;
                    case 2:
                        resultado = sum1 - sum2;
                        Console.Write("How much is " + sum1 + " - " + sum2 + " ? ");
                        user = int.Parse(Console.ReadLine());
                        break;
                }

                if (resultado == user)
                {
                    Aciertos++;
                    total = levelis - Aciertos;
                    score = score + scorear; 
                    Console.WriteLine("You have succeeded, you have " + total + " tests left, and your score is " + score);
                    if (total == 0 && Level == 4)
                    {
                        Console.WriteLine("You have completed the first level of Master Of Calculator. Now you can go to the Middle level if you wish or go to a more advanced one.");
                        Level = 0;
                        MasterCalculator.Inicio menu = new MasterCalculator.Inicio();
                        menu.Menu();
                    } else if(total == 0)
                    {
                        Console.WriteLine("You have completed the first wave of Master Of Calculator.");
                        Level++;
                        MasterCalculator.EasyLevel LevelComplete = new MasterCalculator.EasyLevel();
                        LevelComplete.iniciarLevelEasy(Level);
                        levelcomplete = true;
                    }
                }
                else
                {
                    Console.WriteLine("OH! Too bad, the result was " + resultado);
                    Aciertos++;
                    score = score - scorefail;

                    if (total > 0 && total < 10)
                    {
                        Console.WriteLine("You have failed, do not worry on the easy level you only have 1 hit left, for each failure, you currently have " + total + ". Your score is " + score);
                    }
                    else
                    {
                        Console.WriteLine("End of game, your score has been " + score);
                        Console.ReadLine();
                        MasterCalculator.Inicio eh = new MasterCalculator.Inicio();
                        Console.Clear();
                        eh.Menu();
                    }
                }
            } while (levelcomplete == false);
        }
    }
}
