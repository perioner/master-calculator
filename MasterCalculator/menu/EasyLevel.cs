﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterCalculator
{
    public class EasyLevel
    {
        public void iniciarLevelEasy(int Level)
        {
            MasterCalculator.Practice lvlinit = new MasterCalculator.Practice();
            if (Level == 1)
            {
                Console.WriteLine("Wave 1");
                lvlinit.InitPractice();
            }
            else if (Level == 2)
            {
                Console.WriteLine("Wave 2");
                lvlinit.InitPractice();
                Console.ReadLine();
            }
            else if (Level == 3)
            {
                Console.WriteLine("Wave 3");
                lvlinit.InitPractice();
                Console.ReadLine();
            } else
            {
                Console.WriteLine("Wave 4");
                lvlinit.InitPractice();
                Console.ReadLine();
            }
        }
    }
}
