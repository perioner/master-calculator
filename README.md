# _Master Calculator_
**_Master Calculator_** A simple **_minigame_** written in C # Sharp in which you must compete against the console in different arithmetic operations.

[![Project Version](https://img.shields.io/badge/VERSION-0.1.0-orange.svg?style=for-the-badge&logo=gitlab)](#)
[![Project Status](https://img.shields.io/badge/STATUS-PRE_ALPHA-success.svg?style=for-the-badge&logo=statuspage)](#)

| Requeriments | Description |
| :----------- | :---------- |
| **Windows** | **Vista or Newer** is recommended |
| **Net Framework** | **4.6.2** is recommended |

## Development Team

* @perioner - *Back-End Developer*

